#!/bin/bash

YEAR_START=2018
YEAR_STOP=2018
MONTH_START=1
MONTH_STOP=1

ORIG_DIR="/scratch/shared/awoude//MERRA/"
PROC_DIR="/projects/0/ctdas/awoude/NRT/input/"

#bash getmerra_auto $YEAR_START $MONTH_START $MONTH_END $ORIG_DIR

sed -i "s/startYr=[^,]*/startYr=$YEAR_START/g" merra_pullvar_auto.f90
sed -i "s/stopYr=[^,]*/stopYr=$YEAR_STOP/g" merra_pullvar_auto.f90
sed -i "s/startMon=[^,]*/startMon=$MONTH_START/g" merra_pullvar_auto.f90
sed -i "s/stopMon=[^,]*/stopMon=$MONTH_STOP/g" merra_pullvar_auto.f90
sed -i "s|filedir=[^,]*|filedir=\'$ORIG_DIR\'|g" merra_pullvar_auto.f90
sed -i "s|outdir=[^,]*|outdir=\'$PROC_DIR\'|g" merra_pullvar_auto.f90
#
make merra_pullvar_auto
./merra_pullvar_auto
#
sed -i "s|merradir=[^,]*|merradir=\'$PROC_DIR\'|g" scale_merra_gpcp_auto.f90 
sed -i "s/startYr=[^,]*/startYr=$YEAR_START/g" scale_merra_gpcp_auto.f90
sed -i "s/stopYr=[^,]*/stopYr=$YEAR_STOP/g" scale_merra_gpcp_auto.f90
sed -i "s/startMon=[^,]*/startMon=$MONTH_START/g" scale_merra_gpcp_auto.f90
sed -i "s/stopMon=[^,]*/stopMon=$MONTH_STOP/g" scale_merra_gpcp_auto.f90
#
make scale_merra_gpcp_auto
./scale_merra_gpcp_auto
#
sed -i "s|inputdir=[^,]*|inputdir=\'$PROC_DIR\'|g" merra_pullsib_auto.f90 
sed -i "s|outputdir=[^,]*|outputdir=\'$PROC_DIR\'|g" merra_pullsib_auto.f90 
sed -i "s/startYr=[^,]*/startYr=$YEAR_START/g" merra_pullsib_auto.f90
sed -i "s/stopYr=[^,]*/stopYr=$YEAR_STOP/g" merra_pullsib_auto.f90
sed -i "s/startmon=[^,]*/startmon=$MONTH_START/g" merra_pullsib_auto.f90
sed -i "s/stopmon=[^,]*/stopmon=$MONTH_STOP/g" merra_pullsib_auto.f90
#
make merra_pullsib_auto
./merra_pullsib_auto
