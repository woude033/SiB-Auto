#!/usr/bin/env python
# namel_io.py

"""
Author : Erik
Original Author: Ivar
Revision History:
File created on 06 Oct 2010.
Recoded to support SiB4 - Erik
"""
import shutil
from numpy import *

import os
import glob

num_outputdirs = 288
path_output     = '/scratch/shared/awoude/'
sibdir = '/projects/0/ctdas/NRT/data/SiB/'
path_exec = sibdir + 'mpi/'
sim_name       = 'sib4_NRT'

startyear = 2018
endyear   = 2018

spin   = False
n_spin = 10

minlon = -15
maxlon = 35
minlat = 33
maxlat = 72

dtsibout = -1

if spin == True:
    outputname = 'output_'+sim_name+'_spin_1850'
    execname   = 'ex_dir_'+sim_name+'_spin_1850'
else:
    outputname = 'output_'+sim_name+'_sim'  
    execname   = 'ex_dir_'+sim_name+'_sim'  
    #spinoutput = 'output_'+sim_name+'_spin_1850' 

targdir = os.path.join(path_output, outputname)
# Create output folder
if not os.path.exists(targdir):
    os.makedirs(targdir)


targdir = os.path.join(path_output ,execname)
# Create folders to store executables
if not os.path.exists(targdir):
    os.makedirs(targdir)
    for i in range(num_outputdirs):
        targdir2= os.path.join(targdir,'case_%s'%(i+1))
        if not os.path.exists(targdir2):
            os.makedirs(targdir2)

path = path_exec

oldline = [''] * 12
newline = [''] * 12

orig_namelist = path_exec + 'namel_sibdrv'

# Create namelists for each thread
for j in range(12):
    for i in range(1, num_outputdirs+1):
        if j==0:
            shutil.copyfile(orig_namelist, '%s/namel_%s'%(path,i))
        file = os.path.join(path,'namel_%s'%i)
        
        oldline[1]    ="   out_path = '/scratch/shared/nsmith/NRT/"
        newline[1]    ="   out_path = %s/%s/"%(path_output, outputname) 

        if spin == False:
            oldline[2] ="   ic_file = '/projects/0/ctdas/NRT/data/SiB//restart/sib_r199601p025.nc'"
            newline[2] ="   ic_file = '%s/restart/sib_r199601p%s.nc'" %(sibdir, str(i).zfill(3))
            #newline[2] ="   ic_file = '/scratch/shared/nsmith/NRT/%s/sib_requibp%ss%s.nc" %(spinoutput,str(i).zfill(3),str(n_spin).zfill(2))
        
        oldline[3]  ="   startyear      = 1998," 
        newline[3]  ="   startyear      = %s,"%(str(startyear))
        oldline[4]  ="   endyear        = 2010,"
        newline[4]  ="   endyear        = %s,"%(str(endyear))
        oldline[5]  ="   spinup         = .false.," 
        newline[5]  ="   spinup         = .%s.,"%(str(spin).lower())
        oldline[6]  ="   spinup_maxiter = 5,"
        newline[6]  ="   spinup_maxiter = %s,"%(str(n_spin))
        oldline[7]  ="   minlon = -180,"
        newline[7]  ="   minlon = %s,"%(str(minlon))
        oldline[8]  ="   maxlon = 180," 
        newline[8]  ="   maxlon = %s,"%(str(maxlon)) 
        oldline[9]  ="   minlat = -90," 
        newline[9]  ="   minlat = %s,"%(str(minlat))
        oldline[10] ="   maxlat = 90," 
        newline[10] ="   maxlat = %s,"%(str(maxlat))
        oldline[11] ="   dtsibqp        = -1,"
        newline[11] ="   dtsibqp        = %s,"%(str(dtsibout))

        # Read - write namelists
        f = open(file, "r") 
        text = f.read() 
        f.close() 
        f = open(file, "w") 
        f.write(text.replace(oldline[j], newline[j])) 
        f.close() 

sib4_script_path = path_exec + '/sib4_script'

oldline = 'cd /scratch/shared/kooij032/NRT/ex_dir_global/case_${rank}'
newline = 'cd ' + path_output + execname + '/case_${rank}' 

# Change sib4_script
f = open(sib4_script_path+'.template' , "r")
text = f.read()
f.close()
f = open(sib4_script_path, "w")
f.write(text.replace(oldline, newline))
f.close()

# Change sib4 permissions for mpi ---gives permission error for me (RdK)
#os.chmod(sib4_script_path, 0o777)

print("All went well, the modifications are done")

if __name__ == "__main__":
    pass
