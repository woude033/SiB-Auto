import netCDF4 as nc
import sys
import glob
import os

sys.path.append('/Users/lindakooijmans/Documents/Python/Funcs/')
import sib4maptools as sibtools

outputdir = '/scratch/shared/nsmith/test_near_real_time/sib4_out_test'

for yy in range(2010, 2020): 
    for mm in range(1, 13):
    
        ########################
        ###### Load Data #######
        ########################
        
        filedir = '/scratch/shared/nsmith/NRT/output_sib4_NRT_sim/qsib_'+str(yy)+'{:02}'.format(mm)+'.g.qp2.nc'
        
        files = glob.glob(filedir)
        
        with nc.Dataset(files[0]) as d:
            time_orig = d['time'][:]
            lonsib_orig = d['lonsib'][:]
            latsib_orig = d['latsib'][:]

            gpp_orig       = d['gpp'][:,:]
            nee_orig       = d['nee'][:,:]
            reco_orig      = d['reco'][:,:]

        print(filedir+"   loaded")
        
        ##############################
        ###### Create new file #######
        ##############################
        
        newfiledir = outputdir+'SiB4v2_BioFluxes_'+str(yy)+'{:02}'.format(mm)+'.nc'

        x = nc.Dataset(newfiledir,'w',format='NETCDF4')

        # -- Create dimension

        x.createDimension('latitude', 360)
        x.createDimension('longitude', 720)
        x.createDimension('time', len(time_orig))

        # -- Create variables: Dataset.createVariable(<var_id>, <type>, <dimensions>)
        latsib = x.createVariable('latitude','f',('latitude'))
        lonsib = x.createVariable('longitude','f',('longitude'))
        time = x.createVariable('time','f',('time'))
        gpp = x.createVariable('gpp','f',('time','latitude', 'longitude'))
        reco = x.createVariable('reco','f',('time','latitude', 'longitude'))
        nee = x.createVariable('nee','f',('time','latitude', 'longitude'))

        # -- Write array to file
        latsib[:] = np.arange(-89.75, 90.25, 0.5)
        lonsib[:] = np.arange(-179.75, 180.25, 0.5)
        time[:] = time_orig[:]
        gpp[:,:,:] = sibtools.convert_2d(gpp_orig, latsib_orig, lonsib_orig)
        reco[:,:,:] = sibtools.convert_2d(reco_orig, latsib_orig, lonsib_orig)
        nee[:,:,:] = sibtools.convert_2d(nee_orig, latsib_orig, lonsib_orig)

        # -- Set attributes of variables
        latsib.setncattr('long_name','Latitude')
        lonsib.setncattr('long_name','Longitude')
        time.setncattr('unit','3-hourly time step')
        time.setncattr('long_name','time')
        gpp.setncattr('unit','micromol m-2 s-1')
        gpp.setncattr('long_name','Gross Primary Productivity')
        gpp.setncattr('comment','Positive values are a flux from the atmosphere to the biosphere')
        nee.setncattr('unit','micromol m-2 s-1')
        nee.setncattr('long_name','Net Ecosystem Exchange')
        nee.setncattr('comment','Positive values are a flux from the biosphere to the atmosphere')
        reco.setncattr('unit','micromol m-2 s-1')
        reco.setncattr('long_name','Total Ecosystem Respiration')
        reco.setncattr('comment','Positive values are a flux from the biosphere to the atmosphere')

        # -- Set metadata
        # x.setncattr('creation_date',datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        x.setncattr('release_note','File created by Linda Kooijmans (Wageningen University)')
        x.setncattr('created','September, 2020')

        # -- Close file
        x.close()

        print(newfiledir+"   created")
