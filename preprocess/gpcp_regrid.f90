!Program to redo longitudes of GPCP 
!  to go from -180 to 180, and
!  to create monthly totals rather than month means, and
!  to regrid GPCP to weather data resolution.
!
!Use the makefile in this directory
!>make gpcp_regrid  (NC=3 for netcdf3)
!
!kdhaynes, 01/2018
!
program gpcp_regrid

use netcdf
implicit none

character*180, parameter :: &  !initial GPCP file
   infile='/scratch/shared/erikvs/precip.mon.mean.nc'
character*180, parameter :: &  !regridded GPCP file
   outfile='/scratch/shared/erikvs/gpcp/gpcp_0.625dx0.5d_tot.nc'
character*180, parameter :: &  !regrid file
   regridfile='/scratch/shared/erikvs/merra_2018_proc/merra_201908.nc'

integer, parameter :: gpcpyrstart=1979
real, parameter :: badval = -9999.
integer, parameter :: maxng=4

!!!Netcdf variables
integer :: status
integer :: ncid,ncid2,dimid,varid
integer :: nlatdid, nlondid, ntimedid
integer :: latid, lonid, timeid
integer :: precipid
character*20 :: dimname

!!!GPCP variables
integer :: nlong, nlatg, ntime
real, dimension(:), allocatable :: longpcpin, longpcp, latgpcp
real, dimension(:,:,:), allocatable :: gpcpin, gpcpall, gpcpout
real*8, dimension(:,:), allocatable :: areagg

!!!Regrid variables
integer :: nlon, nlat
real, dimension(:), allocatable :: lon, lat
real*8, dimension(:,:), allocatable :: areag

!!!Areal variables
integer, dimension(:,:), allocatable :: ngmin
integer, dimension(:,:,:), allocatable :: refiin,refjin
real*8, dimension(:), allocatable :: arlonin, arlatin
real*8, dimension(:), allocatable :: arlonout, arlatout
real*8, dimension(:,:,:), allocatable :: contribin
logical :: regridup

!!!Time variables
real, dimension(:), allocatable :: time

!!!Local variables
integer :: i,j,ii,jj
integer :: yrref, monref
integer :: startref, lonref
integer :: numatts, att
character(len=30) :: attname

integer :: dayspermon(12)
real :: radius
real*8 :: pi

!------------------
!Set local variables
data dayspermon/31,28,31,30,31,30,31,31,30,31,30,31/
pi = acos(-1.0)
radius = 6371000.

!------------------
!Get the GPCP data
print*,''
print('(a)'),'GPCP File Information: '
print('(a,a)'), '   ',trim(infile)
status = nf90_open( trim(infile), nf90_nowrite, ncid )

status = nf90_inq_dimid(ncid, 'lon', dimid )
status = nf90_inquire_dimension(ncid, dimid, dimname, nlong)
status = nf90_inq_dimid(ncid, 'lat', dimid )
status = nf90_inquire_dimension(ncid, dimid, dimname, nlatg)
status = nf90_inq_dimid(ncid, 'time', dimid )
status = nf90_inquire_dimension(ncid, dimid, dimname, ntime)

allocate(longpcpin(nlong))
allocate(longpcp(nlong), latgpcp(nlatg))
status = nf90_inq_varid(ncid, 'lon', varid )
status = nf90_get_var(ncid, varid, longpcpin )
status = nf90_inq_varid(ncid, 'lat', varid )
status = nf90_get_var(ncid, varid, latgpcp )

allocate(gpcpin(nlong,nlatg,ntime))
allocate(gpcpall(nlong,nlatg,ntime))
status = nf90_inq_varid(ncid, 'precip', varid )
status = nf90_get_var(ncid, varid, gpcpin )
status = nf90_close(ncid)
where(gpcpin .lt. 0.) gpcpin=0.

!....fix GPCP longitude
IF (maxval(longpcpin) .GT. 180.) THEN
   startref=0
   do i=1,nlong
      if ((longpcpin(i) .gt. 180.) .and. &
          (startref .eq. 0)) then
          startref = i
      endif
   enddo

   lonref = startref
    do i=1,nlong
        longpcp(i) = longpcpin(lonref)
        if (longpcp(i) .gt. 180.) then
            longpcp(i) = longpcp(i) - 360.
        endif
        gpcpall(i,:,:) = gpcpin(lonref,:,:)
        lonref = lonref + 1
         if (lonref .gt. nlong) lonref=1
    enddo

   print*,'  Reversed Lon (min/max): ',minval(longpcp),maxval(longpcp)
   print*,'  Precip (min/max in mm/day): ',minval(gpcpall),maxval(gpcpall)
ELSE
   longpcp=longpcpin
   gpcpall=gpcpin
ENDIF
IF (maxval(longpcp) .GT. 180.) THEN
   print('(a)'),'ERROR CONVERTING LON TO -180 TO 180'
   RETURN
ENDIF

!....get GPCP time
allocate(time(ntime))
monref=1
yrref=gpcpyrstart
do i=1,ntime
   time(i) = yrref + (monref-0.5)/12.
   gpcpall(:,:,i) = gpcpall(:,:,i)*dayspermon(monref)

   monref=monref+1
   if (monref .eq. 13) then
      monref = 1
      yrref=yrref+1
   endif
enddo

!-----------------------
!Get regrid information
print*,''
print('(a)'),'Regrid File Information: '
print('(a,a)'), '   ',trim(regridfile)
status = nf90_open( trim(regridfile), nf90_nowrite, ncid )
if (status .ne. nf90_noerr) then
   print*,'Error Opening File. Stopping.'
   stop
endif

status = nf90_inq_dimid(ncid, 'lon', dimid )
status = nf90_inquire_dimension(ncid, dimid, dimname, nlon)
status = nf90_inq_dimid(ncid, 'lat', dimid )
status = nf90_inquire_dimension(ncid, dimid, dimname, nlat)

allocate(lon(nlon),lat(nlat))
status = nf90_inq_varid(ncid, 'longitude', varid )
status = nf90_get_var(ncid, varid, lon )
status = nf90_inq_varid(ncid, 'latitude', varid )
status = nf90_get_var(ncid, varid, lat )
status = nf90_close(ncid)

regridup = .true.
IF (nlon .GT. nlong) regridup = .false.

!--------------------------------
!Allocate the areal arrays
allocate(arlonin(nlong),arlatin(nlatg))
allocate(arlonout(nlon),arlatout(nlat))
allocate(areagg(nlong,nlatg))
allocate(areag(nlon,nlat))

allocate(gpcpout(nlon,nlat,ntime))
gpcpout = 0.

!-------------------------------
!Regrid the precip data
IF (regridup) THEN
    allocate(ngmin(nlong,nlatg))
    allocate(refiin(nlong,nlatg,maxng), &
             refjin(nlong,nlatg,maxng))
    allocate(contribin(nlong,nlatg,maxng))

    call regrid_upinfo(  &
         nlong, nlatg, &
         nlon, nlat, maxng, &
         longpcp, latgpcp, lon, lat,  &
         ngmin, refiin, refjin,  &
         arlonin, arlatin, arlonout, arlatout, &
         contribin, areagg, areag)

    call regrid_varup( &
         gpcpyrstart,  &
         nlong, nlatg, &
         nlon,nlat, &
         ntime, maxng, &
         lon, lat, &
         ngmin, refiin, refjin, &
         arlonin, arlatin, arlonout, arlatout, &
         contribin, areag, &
         gpcpall, gpcpout)

ELSE
    allocate(ngmin(nlon,nlat))
    allocate(refiin(nlon,nlat,maxng), &
             refjin(nlon,nlat,maxng))
    allocate(contribin(nlon,nlat,maxng))

    call regrid_downinfo(  &
         nlong, nlatg, &
         nlon, nlat, maxng, &
         longpcp, latgpcp, lon, lat, &
         ngmin, refiin, refjin, &
         arlonin, arlatin, arlonout, arlatout, &
         contribin, areagg, areag)

    call regrid_vardown( &
         gpcpyrstart, &
         nlong, nlatg, ntime, &
         nlon, nlat, maxng, &
         ngmin, refiin, refjin, &
         arlonin, arlatin, arlonout, arlatout, &
         contribin, gpcpall, gpcpout)

ENDIF



!!!Write out file
print('(a)')
print('(a)'), 'Writing file: '
print('(2a)'),'  ', trim(outfile)
status = nf90_create( trim(outfile), nf90_clobber, ncid)

!...copy global attributes....
status = nf90_open( trim(infile), nf90_nowrite, ncid2 )
status = nf90_inquire(ncid2, nAttributes=numatts)
do att=1,numatts
   status = nf90_inq_attname(ncid2,nf90_global,att,attname)
   status = nf90_copy_att(ncid2,nf90_global,trim(attname),ncid,nf90_global)
enddo
status = nf90_close(ncid2)

!...dimensions...
status = nf90_def_dim( ncid, 'lon', nlon, nlondid )
status = nf90_def_dim( ncid, 'lat', nlat, nlatdid )
status = nf90_def_dim( ncid, 'time', ntime, ntimedid )

!...variables...
status = nf90_def_var( ncid,'lon',nf90_float,(/nlondid/), lonid )
status = nf90_put_att( ncid, lonid, 'units','degrees_north')

status = nf90_def_var( ncid,'lat',nf90_float,(/nlatdid/), latid )
status = nf90_put_att( ncid, latid, 'units','degrees_east')

status = nf90_def_var( ncid,'time',nf90_float,(/ntimedid/), timeid )
status = nf90_put_att( ncid, timeid, 'units','years since 0000-00-00')

status = nf90_def_var( ncid,'precip',nf90_float,(/nlondid,nlatdid,ntimedid/), precipid )
status = nf90_put_att( ncid, precipid, 'long_name', &
         'Total Month Precipitation' )
status = nf90_put_att( ncid, precipid, 'units', 'mm/month')
status = nf90_put_att( ncid, precipid, 'dataset', &
         'GPCP Version 2.3 Combined Precipitation Dataset')

!...take file out of define mode, into data mode
status = nf90_enddef( ncid )

!...load the variables...
status = nf90_put_var( ncid, lonid, lon )
status = nf90_put_var( ncid, latid, lat )
status = nf90_put_var( ncid, timeid, time )
status = nf90_put_var( ncid, precipid, gpcpout)

!...close the file
status = nf90_close( ncid )

print*,''
print('(a)'),'Finished Processing.'
print*,''

end program gpcp_regrid
