##### Extract Equi ########
'''
Extract equilibrium files and copy them
to new directory. This circumvents the problem
that some cores achieve equilibrium earlier than
other, and therefore the filesnames can be
different.
'''

# Experiment name (without _spin or _final)
exp_name = 'sib4_NRT'
dir_name = '/scratch/shared/kooij032/NRT/'
n_cores  = 288
n_iter   = 10

# Load libraries
import glob
import os
import shutil
import numpy as np

# Move equip folder
equip_dir = dir_name+'output_'+exp_name+'_spin_1850'
shutil.move(equip_dir, equip_dir+ '_MOVE/')

# Create new equip folder
if not os.path.exists(equip_dir):
    os.mkdir(equip_dir)

# Load file list
flist = sorted(glob.glob(equip_dir+'_MOVE/*'))

# Initialize loop variables
flist_id = 1
eq_list = np.zeros(288, dtype=int)

# Loop over all files and determine last equip file per core
#print(len(flist))

for i in range(len(flist)):
    if flist[i][-9:-6] != "%03d" % (flist_id,):
        eq_list[flist_id-1] = i-1
        flist_id += 1

# Make sure the last core is considered
eq_list[flist_id-1] = len(flist) -1


#print(len(flist) -1)
#print(flist_id)
#print(eq_list)

# Copy last equip file to spin folder
n_iter_str = 's' + "%02d" % (n_iter,) + '.nc'
for i in range(288):
    print(flist[eq_list[i]])
    shutil.copyfile(flist[eq_list[i]], equip_dir+'/sib_requibp'+"%03d" % (i+1,)+n_iter_str)
    print('FILE COPIED')
    print('old: '+flist[eq_list[i]])
    print('new: '+equip_dir+'/sib_requibp'+"%03d" % (i+1,)+n_iter_str)
