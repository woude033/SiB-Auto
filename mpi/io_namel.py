#!/usr/bin/env python
# namel_io.py

"""
Author : Erik
Original Author: Ivar
Revision History:
File created on 06 Oct 2010.
Recoded to support SiB4 - Erik
"""
import shutil
from numpy import *

import os
import glob

num_outputdirs = 288
path_ouput     = '/scratch/shared/awoude/'
sim_name       = 'sib4_NRT'

startyear = 1997 
endyear   = 2020

spin   = False
n_spin = 10

minlon = -15
maxlon = 35
minlat = 33
maxlat = 72

dtsibout = -1

if spin == True:
    outputname = 'output_'+sim_name+'_spin_1850'
    execname   = 'ex_dir_'+sim_name+'_spin_1850'
else:
    outputname = 'output_'+sim_name+'_sim'  
    execname   = 'ex_dir_'+sim_name+'_sim'  
    #spinoutput = 'output_'+sim_name+'_spin_1850' 

targdir = os.path.join(path_ouput,outputname)

# Create output folder
if not os.path.exists(targdir):
    os.makedirs(targdir)

path_exec = '/scratch/shared/awoude/'

targdir = os.path.join(path_exec,execname)

# Create folders to store executables
if not os.path.exists(targdir):
    os.makedirs(targdir)
    for i in range(num_outputdirs):
        targdir2= os.path.join(targdir,'case_%s'%(i+1))
        if not os.path.exists(targdir2):
            os.makedirs(targdir2)

path ='/projects/0/ctdas/NRT/data/SiB/mpi'

oldline = [''] * 12
newline = [''] * 12

orig_namelist = '/projects/0/ctdas/NRT/data/SiB/mpi/namel_sibdrv'

# Create namelists for each thread
for j in range(12):
    for i in range(num_outputdirs):
        i=i+1
        if j==0:
            shutil.copyfile(orig_namelist, '%s/namel_%s'%(path,i))
        file = os.path.join(path,'namel_%s'%i)
        
        oldline[1]    ="   out_path = '/scratch/shared/awoude/"
        newline[1]    ="   out_path= '/scratch/shared/awoude/%s/" %(outputname)

        if spin == False:
            oldline[2] ="   ic_file = '/projects/0/ctdas/sib4_input/requib/sib_requib2.nc"
            newline[2] ="   ic_file = '/projects/0/ctdas/NRT/data/SiB/restart/sib_r199601p%s.nc" %(str(i).zfill(3))
            #newline[2] ="   ic_file = '/scratch/shared/kooij032/NRT/%s/sib_requibp%ss%s.nc" %(spinoutput,str(i).zfill(3),str(n_spin).zfill(2))
        
        oldline[3]  ="   startyear      = 1998," 
        newline[3]  ="   startyear      = %s,"%(str(startyear))
        oldline[4]  ="   endyear        = 2010,"
        newline[4]  ="   endyear        = %s,"%(str(endyear))
        oldline[5]  ="   spinup         = .false.," 
        newline[5]  ="   spinup         = .%s.,"%(str(spin).lower())
        oldline[6]  ="   spinup_maxiter = 5,"
        newline[6]  ="   spinup_maxiter = %s,"%(str(n_spin))
        oldline[7]  ="   minlon = -180,"
        newline[7]  ="   minlon = %s,"%(str(minlon))
        oldline[8]  ="   maxlon = 180," 
        newline[8]  ="   maxlon = %s,"%(str(maxlon)) 
        oldline[9]  ="   minlat = -90," 
        newline[9]  ="   minlat = %s,"%(str(minlat))
        oldline[10] ="   maxlat = 90," 
        newline[10] ="   maxlat = %s,"%(str(maxlat))
        oldline[11] ="   dtsibqp        = -1,"
        newline[11] ="   dtsibqp        = %s,"%(str(dtsibout))

        # Read - write namelists
        f = open(file, "r") 
        text = f.read() 
        f.close() 
        f = open(file, "w") 
        f.write(text.replace(oldline[j], newline[j])) 
        f.close() 

sib4_script_path = '/projects/0/ctdas/NRT/data/SiB/mpi/sib4_script'

oldline = 'cd /scratch/shared/kooij032/NRT/ex_dir_global/case_${rank}'
newline = 'cd /scratch/shared/awoude/'+execname+'/case_${rank}'

# Change sib4_script
f = open(sib4_script_path+'.template' , "r")
text = f.read()
f.close()
f = open(sib4_script_path, "w")
f.write(text.replace(oldline, newline))
f.close()

# Change sib4 permissions for mpi
#os.chmod('/projects/0/ctdas/NRT/data/SiB/mpi/sib4_script', 0o777)

print("All went well, the modifications are done")

if __name__ == "__main__":
    pass
