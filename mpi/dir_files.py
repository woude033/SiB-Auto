'''
show files in directory
'''
# Load libraries
import glob

import os
import shutil
import numpy as np

dir = '/scratch/shared/kooij032/NRT/output_sib4_NRT_final/'

# Load all files in directory
flist = sorted(glob.glob(dir+'/*'))

# Show file list
print(flist)
