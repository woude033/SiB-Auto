
$CONTROL_LIST_SIBDRV
   nsib           = 62521,
   starttime      = 1,
   startyear      = 1998,
   endtime        = 365,
   endyear        = 2010,
   dtsib          = 600,
   restart_dtsib  = -12,
   qp_dtsib       = 10800,
   pbp_dtsib      = 0,
   hr_dtsib       = 0,
/

$IO_LIST_SIBDRV
   pft_info  = '/projects/0/ctdas/NRT/data/SiB/sib4v2/input/params/info_pft.dat'
   pool_info = '/projects/0/ctdas/NRT/data/SiB/sib4v2/input/params/info_pool.dat'
   aero_file = '/projects/0/ctdas/NRT/data/SiB/sib4v2/input/params/sib_aero.nc'
   pgdd_file = '/projects/0/ctdas/NRT/data/SiB/sib4v2/input/params/phen_gdd.dat'
   pstg_file = '/projects/0/ctdas/NRT/data/SiB/sib4v2/input/params/phen_stg_EurDrought.dat'
   phys_file = '/projects/0/ctdas/NRT/data/SiB/sib4v2/input/params/sib_phys_EurDrought.dat'
   pool_file = '/projects/0/ctdas/NRT/data/SiB/sib4v2/input/params/sib_pool.dat'
   vs_file = '/projects/0/ctdas/sib4_input/vs/sib_vs_0.5deg.nc'
   ic_file = '/projects/0/ctdas/sib4_input/requib/sib_requib2.nc'
   dr_path = '/projects/0/ctdas/sib4_input/meteo/merra/merra2_0.5deg_'
   tm5_path = '/projects/0/ctdas/linda/sib4_input/TM5MR/'
   fr_path = '../../sib4_driver/global/gfed4/gfed4_0.5deg_'
   out_path = '/scratch/shared/rdkok/NRT/'
   out_info = '/projects/0/ctdas/NRT/data/SiB/sib4v2/input/sib_outopts_NRT'
   out_rinfo= '/projects/0/ctdas/NRT/data/SiB/sib4v2/input/sibr_outopts'
/

$SPINUP_LIST_SIBDRV
   spinup         = .false.,
   spinup_default = .true.,
   spinup_numyrs  = 1,
   spinup_maxiter = 10,
   spinup_threshold = 0.01,
   spinup_writediag = .false.,
   spinup_writetxtf = .false.,
/

$SUBGRID_SIBDRV
   minlon = -180,
   maxlon = 180,
   minlat = -90,
   maxlat = 90,
/

$PBP_LIST_SIBDRV
   npbp = 0
/
-lon-, -lat-,
/

$BALAN_LIST_SIBDRV
   badtc_print    = .false.,
   badtc_stop     = .false.,
   bnum_allow     = 10,
   canb_print     = .false.,
   canb_stop      = .false.,
   canb_thresh    = 1.E-6,
   carbonb_print  = .false.,
   carbonb_stop   = .false.,
   carbonb_thresh = 1.E-6,
   fireb_print    = .false.,
   fireb_stop     = .false.,
   fireb_thresh   = 1.E-6,
   snocmbn_print  = .false.,
   snocmbn_stop   = .false.,
   snocmbn_thresh = 1.E-6,
   energyb_print  = .false.,
   energyb_stop   = .true.,
   energyb_thresh = 0.01,
   waterb_print   = .false.,
   waterb_stop    = .true.,
   waterb_thresh  = 0.5,
/

$PRINT_LIST_SIBDRV
   print_avec    = .false.,
   print_driver  = .false.,
   print_fire    = .false.,
   print_harvest = .false.,
   print_pftinfo = .false.,
   print_pooll   = .false.,
   print_soil    = .false.,
   print_sscol   = .false.,
   print_veg     = .false.,
   print_stop    = .false.,
/

$SWITCH_LIST_SIBDRV
   cornsoy_switch   = .false.,
   fire_switch      = .false.,
   grazing_switch   = .true.,
   green_switch     = .true.,
   eqclear_switch   = .true.,
   leapyr_switch    = .false.,
   updatelst_switch  = .true.,
   tm5mr_switch     = .false.,
   soilogee_switch  = .false.,
/

------------------------------------------------------------------------------
  nsib      : number of points in simulation
  starttime : day of year to start simulation
  startyear : year to start simulation
  endtime   : day of year to stop simulation
  endyear   : year to stop simulation
  dtsib     : prognostic time step, in seconds
  dtsibrestart  : greater than zero implies units of seconds - restart output interval
                  less than zero implies units of months - restart output interval
  dtsibqp  : greater than zero implies units of seconds - QP output interval
             less than zero implies units of months - QP output interval
  dtsibpbp : greater than zero implies units of seconds - PBP output interval
             less than zero implies units of months - PBP output interval
  dtsibhr  : greater than zero implies units of seconds - HR output interval
             less than zero implies units of months - HR output interval

  pft_info  : File containing PFT information
  pool_info : File containing pool information
  aero_file : File containing aerodynamical parameters
  pgdd_file : File containing GDD-based phenological parameters
  pstg_file : File containing stage-based phenological parameters
  pnvg_file : File containing non-vegetation parameters
  phys_file : File containing physiological parameters
  pool_file : File containing pool respiration/transfer parameters
  vs_file   : File containing vegetation structure
  ic_file   : File containing initial condition/restart values
  dr_path   : Directory containing driver data
  fr_path   : Directory containing fire emissions
  out_path  : Directory for model output
  out_info  : File containing output specification/information
  out_rinfo : File containing restart specification/information

  spinup    : Flag to perform a spinup simulation
              ->simulation continues until either:
                 -- pools are within threshold equilibrium values
                 -- max number of iterations is met
  spinup_default : Flag to use default initial conditions for spin-up run
                    -- all pools zero except minimal froot
                    -- soil moisture starts at saturation
  spinup_numyrs  : Number of years in spinup run to run before calculating equilibrium pools
  spinup_maxiter : Number of interations in a spinup simulation
  spinup_threshold: Input/output ratio threshold for 'spun-up'
  spinup_writediag: Flag to follow output choices in spin-up run
                      (false will only save equilibrium files)
  spinup_writetxtf: Flag to write equilibrium pools to a text file

  minlon : Minimum longitude for setting a subgrid
  maxlon : Maximum longitude for setting a subgrid
  minlat : Minimum latitude for setting a subgrid
  maxlat : Maximum latitude for setting a subgrid

  npbp : Number of PBP's to be saved
          --corresponding lon/lat pairs (in degrees) are listed below npbp
          --value of -1 will save all points

  badtc_print    : print canopy temperatures?
  badtc_stop     : stop for bad canopy temperatures?
  bnum_allow     : number of allowable balance offenses (i4)
  canb_print     : print canopy balance values?
  canb_stop      : stop if canopy balance fails?
  canb_thresh    : canopy balance threshold (r4)
  carbonb_print  : print carbon balance information?
  carbonb_stop   : stop if carbon balance fails?
  carbonb_thres  : carbon balance threshold (r4)
  fireb_print    : print fire balance information?
  fireb_stop     : stop if fire balance fails?
  fireb_thres    : fire balance threshold (r4)
  snocmbn_print  : print snow combine information?
  snocmbn_stop   : stop if snow combine water balance fails?
  snocmbn_thresh : snow combine balance threshold (r4)
  energyb_print  : print energy balance values?
  energyb_stop   : stop if energy balance fails?
  energyb_thresh : energy balance threshold (r4)
  waterb_print   : print water balance values?
  waterb_stop    : stop if water balance fails?
  waterb_thresh  : water balance threshold (r4)

  print_avec     : print avec/bvec values?
  print_driver   : print driver data?
  print_fire     : print fire emissions?
  print_harvest  : print harvest information?
  print_pftinfo  : print PFT information?
  print_pooll    : print live pool values?
  print_soil     ; print soil properties?
  print_sscol    : print soil/snow layer info?
  print_veg      : print vegetation values?
  print_stop     : stop after printing information?

  cornsoy_switch : Flag to annually alternate corn/soybeans
  fire_switch    : Flag for fire emissions
  grazing_switch : Flag for grazing
  green_switch   : Flag to use greenness fraction
  eqclear_switch : Flag to clear equilibrium variables
                     at start of simulation
  leapyr_switch  : Flag for using leap years (.F. == constant 365 days)
  updatelst_switch: Flag to update carbon pools at 0 LST (rather than 0 GMT)

=============================
Julian Days (not leap year )
  January    1 = 1
  February   1 = 32
  March      1 = 60
  April      1 = 91
  May        1 = 121
  June       1 = 152
  July       1 = 182
  August     1 = 213
  September  1 = 244
  October    1 = 274
  November   1 = 305
  December   1 = 335
=============================
